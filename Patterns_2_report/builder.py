class Car(object):
    def __init__(self, wheels=4, seats=4, engine="V4"):
        self.wheels = wheels
        self.seats = seats
        self.engine = engine

    def __str__(self):
        return "This is a {0} car with {1} wheels and {2} seats.".format(
            self.engine, self.wheels, self.seats
        )


class CarBuilder:
    def set_wheels(self):
        pass

    def set_seats(self):
        pass

    def set_engine(self):
        pass

    def get_result(self):
        pass


class RegularCarBuilder(CarBuilder):
    def __init__(self):
        self.car = Car()

    def set_wheels(self):
        self.car.wheels = 4
        return self

    def set_engine(self):
        self.car.engine = "V4"
        return self

    def set_seats(self):
        self.car.seats = 4
        return self

    def get_result(self):
        return self.car


class BusBuilder(CarBuilder):
    def __init__(self):
        self.car = Car()

    def set_wheels(self):
        self.car.wheels = 6
        return self

    def set_engine(self):
        self.car.engine = "V8"
        return self

    def set_seats(self):
        self.car.seats = 10
        return self

    def get_result(self):
        return self.car


class CarBuilderDirector(object):
    def __init__(self, builder):
        self.concrete_builder = builder()

    def set_builder(self, builder):
        self.concrete_builder = builder()

    def construct(self):
        return self.concrete_builder\
            .set_wheels()\
            .set_seats()\
            .set_engine()\
            .get_result()


director = CarBuilderDirector(RegularCarBuilder)
print(director.construct())

director.set_builder(BusBuilder)
print(director.construct())
