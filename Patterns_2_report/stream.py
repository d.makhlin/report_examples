# Funny example
# class Suicide:
#     def __init__(self):
#         self.hp = 10
#
#     def damage_self(self):
#         if self.hp > 0:
#             self.hp -= 1
#         else:
#             self.hp = 0
#         return self
#
#     def heal_self(self):
#         self.hp += 1
#         return self
#
#
# bob = Suicide()
# current_day = 0
# while bob.hp != 0:
#     bob.damage_self().heal_self().damage_self()
#     print("Day {0}: Bob has {1} hp.".format(current_day, bob.hp))
#     current_day += 1


class MyList:
    def __init__(self):
        self.elements = list()

    def add_elements(self, elements):
        self.elements += elements

    def filter_over(self, cond):
        new_elements = list()
        for i in self.elements:
            if i <= cond:
                new_elements.append(i)
        self.elements = new_elements.copy()
        return self

    def filter_under(self, cond):
        new_elements = list()
        for i in self.elements:
            if i >= cond:
                new_elements.append(i)
        self.elements = new_elements.copy()
        return self

    def filter_eq(self, cond):
        new_elements = list()
        for i in self.elements:
            if i != cond:
                new_elements.append(i)
        self.elements = new_elements.copy()
        return self


gg = MyList()
gg.add_elements([1, 2, 3, 4, 5, 6])
gg.filter_over(5).filter_under(2).filter_eq(4)
print(gg.elements)
