class EventChannel:
    def __init__(self):
        self.subscribers = list()

    def subscribe(self, sub):
        self.subscribers.append(sub)

    def publish(self, *args):
        for sub in self.subscribers:
            sub.on_event(args)


# Subscribers
class Subscriber:
    def connect(self, channel):
        channel.subscribe(self)

    def on_event(self, *args):
        pass


class MessageWriter(Subscriber):
    def __init__(self, writer_id):
        self.identifier = writer_id

    def on_event(self, *args):
        for arg in args:
            print(arg, end=" ")
        print(" Catched by ", self.identifier)


# Publishers
class Publisher:
    def send(self, channel, *args):
        channel.publish(*args)


if __name__ == "__main__":
    channel = EventChannel()

    sub_1 = MessageWriter("[Sub_1]")
    sub_2 = MessageWriter("[Sub_2]")

    pub_1 = Publisher()
    pub_2 = Publisher()

    sub_1.connect(channel)
    sub_2.connect(channel)

    # Example input
    message = input("Message: ")
    pub_1.send(channel, "[Pub_1]", message)

    message = input("Message: ")
    pub_2.send(channel, "[Pub_2]", message)
