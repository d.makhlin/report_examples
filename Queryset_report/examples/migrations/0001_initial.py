# Generated by Django 2.1.3 on 2018-12-24 09:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='League',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('title', models.TextField(unique=True)),
                ('url', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('league', models.TextField()),
                ('date', models.DateField()),
                ('result', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='MatchCash',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('league', models.TextField()),
                ('date', models.DateField()),
                ('result', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('title', models.TextField(unique=True)),
                ('url', models.TextField(null=True)),
                ('img', models.TextField(null=True)),
                ('league', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='examples.League')),
            ],
        ),
        migrations.AddField(
            model_name='matchcash',
            name='teamA_cash',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='team_a_cash', to='examples.Team'),
        ),
        migrations.AddField(
            model_name='matchcash',
            name='teamB_cash',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='team_b_cash', to='examples.Team'),
        ),
        migrations.AddField(
            model_name='match',
            name='teamA',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='team_a', to='examples.Team'),
        ),
        migrations.AddField(
            model_name='match',
            name='teamB',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='team_b', to='examples.Team'),
        ),
    ]
