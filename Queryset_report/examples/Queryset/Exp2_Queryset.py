import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'Report_examples.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from django.db import models
from Queryset_report.examples.models import Match

# You can use both keys and values while enumerate with zip()
a = [1, 2, 3]
b = [4, 5, 6]
for num1, num2 in zip(a, b):
    print(num1*num2)

# 'epl_result' contains all matches of Manchester United in Premier League.
query = Match.objects.filter(models.Q(teamA_id=32))
epl_result = query.values("id").filter(league='Premier League')

# 'all_result' contains all matches of Manchester United.
all_result = Match.objects.filter(models.Q(teamA_id=32))

# You cannot use both querysets' items while enumerate in zip()
for item1, item2 in zip(epl_result, all_result):
    print(item1.league, item2.league)
    if item1.league == item2.league:
        print("Equal: ", item1.date, item1.teamA.title, item1.result, item1.teamB.title, item1.league, flush=True)
    else:
        print("Different: ", item1.date, item1.teamA.title, item1.result, item1.teamB.title, item1.league, flush=True)
