import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'Report_examples.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from django.db import models
from Queryset_report.examples.models import Match
from django.db.models.functions import Concat


# Orders objects of queryset by amount of their teams' identifiers.
def concat_int():

    query = Match.objects.filter(models.Q(teamA_id=13) | models.Q(teamA_id=15))
    result = query.filter(league='Premier League').annotate(fieldsum=models.F('teamA') + models.F('teamB')).order_by('fieldsum')
    for item in result:
        print(item.date, item.teamA.title, item.result, item.teamB.title, item.teamA, item.teamB, item.league, flush=True)
    print(result.query)


# Orders objects of queryset by strings of teams' concatenated titles.
def concat_str():
    query2 = Match.objects.filter(league='Premier League')
    result2 = query2.annotate(fieldcon=Concat('teamA__title', 'teamB__title')).order_by('fieldcon')

    for item in result2:
        print(item.id, item.date, item.teamA.title, item.teamA, item.result, item.teamB.title, item.teamB, item.league, flush=True)

