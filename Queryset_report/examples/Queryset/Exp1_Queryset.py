import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'Report_examples.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from django.db import models
from Queryset_report.examples.models import Match


def my_slice():

    my_query = Match.objects.all()[1:5]
    print(type(my_query))
    for item in my_query:
        print(item.date, item.teamA.title, item.result, item.teamB.title, item.league)

    my_query = Match.objects.filter(teamA_id=32)[1:5]
    print(type(my_query))
    for item in my_query:
        print(item.date, item.teamA.title, item.result, item.teamB.title, item.league)


def my_upd():
    # 'query' and 'new_query' contain matches with Manchester United as teamA
    query = Match.objects.filter(models.Q(teamA_id=32))

    for item in query:

        # Queryset slice returns list(), so you cannot update it.
        new_query = query[0:2]
        new_query.all().update(league="new_query league")

        # Uploading database data with using 'save' method makes updating in a cycle possible.
        item.league = "save_league"
        item.save()

        # You can add a fields to models during performing a cycle and it wouldn't change object's hash.
        item.add_field=12
        print(item.date, item.teamA.title, item.result, item.teamB.title, item.league, item.field)

        # Either 'update' or 'select_for_update' wouldn't perform in the cycle
        # both methods will change database only after the cycle's end.
        new_query.all().update(league="new league")
        print(item.date, item.teamA.title, item.result, item.teamB.title, item.league, item.add_field)


