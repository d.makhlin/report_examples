import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'Report_examples.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from Queryset_report.examples.models import Match

# Django ORM lets you to perform only LEFT OUTER JOIN or INNER JOIN with 'select_related'.


# <Model>.<manager>.raw() lets you to create raw SQL requests.
def raw():
    league = "Premier League"
    my_raw = Match.objects.raw('SELECT * '
                            'FROM polls_match '
                            'JOIN polls_league ON polls_match.league = polls_league.title '
                            'WHERE league = %s', [league])
    for item in my_raw:
        print(item.date, item.teamA.title, item.result, item.teamB.title, item.league, item.url, flush=True)


# INNER JOIN
# <ForeignKey1>__<ForeignKey2>__<Field> makes inner join between tables with <ForeignKey1> and <ForeignKey2>
def inner():
    inn = Match.objects.select_related('teamA').filter(teamB__league__id='2')
    for item in inn:
        print(item.date, item.teamA.title, item.result, item.teamB.title, item.league, flush=True)
    print(inn.query)


# LEFT JOIN
# select_related(<ForeignKey>) makes left join between table with <ForeignKey> and the table, linked by <ForeignKey>
def left():
    lef = Match.objects.select_related('teamA').filter(league='Premier League')
    for item in lef:
        print(item.date, item.teamA.title, item.result, item.teamB.title, item.league, flush=True)
    print(lef.query)
