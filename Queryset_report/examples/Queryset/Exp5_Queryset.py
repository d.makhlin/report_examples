import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'Report_examples.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from Queryset_report.examples.models import Match
from django.db.models.functions import Coalesce, Greatest, Least, Lower, Now, Substr, Upper

# Django database functions.


# Coalesce
# Accepts a list of at least two field names or expressions and returns the first non-null value.
def my_coalesce():
    query = Match.objects.filter(teamA_id='32').annotate(has_img=Coalesce('teamB__img', 'teamB__title'))
    for item in query:
        print(item.has_img)


# Greatest
# Accepts a list of at least two field names or expressions and returns the greatest value.
# PostgreSQL: Greatest will return the largest non-null expression, or null if all expressions are null.
# SQLite, Oracle, and MySQL: If any expression is null, Greatest will return null.
def my_greatest():
    query = Match.objects.filter(teamA_id='32').annotate(max_id=Greatest('teamA_id','teamB_id'))
    for item in query:
        print(item.max_id)


# Least
# Accepts a list of at least two field names or expressions and returns the least value.
def my_least():
    query = Match.objects.filter(teamA_id='32').annotate(min_id=Least('teamA_id', 'teamB_id'))
    for item in query:
        print(item.min_id)


# Lower
# Accepts a single text field or expression and returns the lowercase representation.
def my_lower():
    query = Match.objects.filter(teamA_id='32').annotate(low_teamB_title=Lower('teamB__title'))
    for item in query:
        print(item.low_teamB_title)


# Upper
# Accepts a single text field or expression and returns the uppercase representation.
def my_upper():
    query = Match.objects.filter(teamA_id='32').annotate(up_teamB_title=Upper('teamB__title'))
    for item in query:
        print(item.up_teamB_title)


# Now
# Returns the database server’s current date and time when the query is executed.
def my_now():
    query = Match.objects.filter(teamA_id='32').annotate(curr_time=Now())
    for item in query:
        print(item.curr_time)


# Substr
# Returns a substring of length from the field or expression starting at position pos.
def my_substr():
    query = Match.objects.filter(teamA_id='32').annotate(sub_title=Substr('teamB__title', 1, 6))
    for item in query:
        print(item.sub_title)
