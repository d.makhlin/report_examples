from django.db import models


class League(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.TextField(unique=True)
    url = models.TextField()


class Team(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.TextField(unique=True)
    url = models.TextField(null=True)
    img = models.TextField(null=True)
    league = models.ForeignKey(League, models.SET_NULL, null=True)


class Match(models.Model):
    id = models.IntegerField(primary_key=True)
    league = models.TextField()
    date = models.DateField()
    result = models.TextField()
    teamA = models.ForeignKey(Team, models.SET_NULL, null=True, related_name="team_a")
    teamB = models.ForeignKey(Team, models.SET_NULL, null=True, related_name="team_b")


class MatchCash(models.Model):
    id = models.IntegerField(primary_key=True)
    league = models.TextField()
    date = models.DateField()
    result = models.TextField()
    teamA_cash = models.ForeignKey(Team, models.SET_NULL, null=True, related_name="team_a_cash")
    teamB_cash = models.ForeignKey(Team, models.SET_NULL, null=True, related_name="team_b_cash")


