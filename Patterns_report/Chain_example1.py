def handle_int(data):
    try:
        int(data)
        print('int')
        return True
    except ValueError:
        print('not int')
        return False


def handle_float(data):
    try:
        float(data)
        print('float')
        return True
    except ValueError:
        print('not float')
        return False


def handle_bool(data):
    try:
        bool(data)
        print('bool')
        return True
    except ValueError:
        print('not bool')
        return False


class Data:
    def __init__(self, user_data):
        self.handlers = []
        self.data = user_data

    def add_handler(self, handler):
        self.handlers.append(handler)

    def handle_data(self):
        for handler in self.handlers:
            if handler(self.data):
                break


if __name__ == '__main__':
    handlers = [handle_int, handle_float, handle_bool]
    my_data = Data('23.1')

    for handle in handlers:
        my_data.add_handler(handle)
    my_data.handle_data()
