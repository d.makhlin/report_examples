class People(object):
    tool = None

    def __init__(self, name):
        self.name = name

    def set_tool(self, tool):
        self.tool = tool

    def write(self, text):
        self.tool.write(self.name, text)


class ToolBase:
    """
    Семейство алгоритмов `Инструмент написания`
    """

    def write(self, name, text):
        raise NotImplementedError()


class PenTool(ToolBase):
    """Ручка"""

    def write(self, name, text):
        print('%s (ручкой) %s' % (name, text))


class BrushTool(ToolBase):
    """Кисть"""

    def write(self, name, text):
        print('%s (кистью) %s' % (name, text))


class Student(People):
    """Студент"""
    tool = PenTool()


class Painter(People):
    """Художник"""
    tool = BrushTool()


if __name__ == '__main__':

    nikita = Student('Никита')
    nikita.write('Пишу лекцию о паттерне Стратегия')

    dima = Painter('Дима')
    dima.write('Рисую иллюстрацию к паттерну Стратегия')

    # Дима решил стать студентом
    dima.set_tool(PenTool())
    dima.write('Нет, уж лучше я напишу конспект')
