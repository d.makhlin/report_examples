import asyncio
import time


async def foo():
    print('Running in foo')
    await asyncio.sleep(3)
    print('Explicit context switch to foo again')


async def bar():
    print('Explicit context to bar')
    await asyncio.sleep(1)
    print('Implicit context switch back to bar')


ioloop = asyncio.get_event_loop()
tasks = [ioloop.create_task(foo()), ioloop.create_task(bar())]
wait_tasks = asyncio.wait(tasks)
a = time.time()
ioloop.run_until_complete(wait_tasks)
print("End ->", time.time() - a)
ioloop.close()