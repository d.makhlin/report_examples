class HasMethod1(object):
    def method_2(self):
        return 1


class HasMethod2(object):
    def method(self):
        return 2


class UsesMethod10(object):
    def uses_method(self):
        return self.method() + 10


class UsesMethod20(object):
    def uses_method(self):
        return self.method() + 20


class C1_10(HasMethod1, UsesMethod10): pass
# class C1_20(HasMethod1, UsesMethod20): pass
# class C2_10(HasMethod2, UsesMethod10): pass
# class C2_20(HasMethod2, UsesMethod20): pass


assert C1_10().uses_method() == 11
# assert C1_20().uses_method() == 21
# assert C2_10().uses_method() == 12
# assert C2_20().uses_method() == 22