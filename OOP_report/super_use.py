class Base:
    def method(self):
        print("Hi i am base method")


class NewClass(Base):
    def method_2(self):
        super(NewClass, self).method()
        print("Hi from NewClass")


obj = NewClass()
obj.method()
print("**********")
obj.method_2()
