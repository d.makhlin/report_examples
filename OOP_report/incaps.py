class User:
    def __init__(self):
        self.login = "MyLogin"
        self._password = "1234"
        self.__age = 12

    def set_pass(self, password):
        self._password = password


print("Step 1")
user = User()
print(user.login)
try:
    print(user.password)
except AttributeError:
    print("No field password")

print(user._password)

print("\nStep 2")
try:
    print(user.__age)
except AttributeError:
    print("No field __age")

print(user._User__age)

