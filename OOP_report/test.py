
class Person(models.Model):
    name = models.CharField(max_length=30)
    age = models.IntegerField()

# Однако если вы выполните следующий код:
  guy = Person(name='bob', age='35')
  print(guy.age)

# Получите не IntegerField, а int