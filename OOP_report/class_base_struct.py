class Cat:

    ancestors = "wild cats"
    tricks = list()

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Name {0}, ancestors {1}".format(self.name, self.ancestors)

    def add_trick(self, trick):
        self.tricks.append(trick)

# INIT START
bob = Cat("Bob")
alice = Cat("alice")
# INIT END

print("Before string update")
print(bob)
print(alice)

# UPDATE START
# kate = Cat("Kate")
# alice.name = "Alice"

Cat.ancestors = "tigers"
Cat.name = "Purge"
kate = Cat("Kate")
# UPDATE END

print("\nAfter string update")
print(bob)
print(alice)
print(kate)

print("Before list update")
print(kate.tricks)

# UPDATE START
kate.add_trick("roll")
alice.add_trick("jump")
bob.add_trick("run on rope")
# UPDATE END

print("Before list update")
print(kate.tricks)
