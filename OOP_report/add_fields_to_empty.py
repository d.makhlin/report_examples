def hello(name):
    print("Hello " + str(name))


class EmptyDataClass:
    pass


data = EmptyDataClass()
# data.method_1("Nikita")
# print(data.field_1)

data.method_1 = hello
data.field_1 = 1
data.field_2 = 2

print(data.field_1)
print(data.field_2)


