class MyMeta(type):
    def __new__(meta, name, bases, dct):
        print('-----------------------------------')
        print("Allocating memory for class", name)
        print(meta)
        print(bases)
        print(dct)
        return super(MyMeta, meta).__new__(meta, name, bases, dct)

    def __init__(cls, name, bases, dct):
        print('-----------------------------------')
        print("Initializing class", name)
        print(cls)
        print(bases)
        print(dct)
        super(MyMeta, cls).__init__(name, bases, dct)


class MyClass(object, metaclass=MyMeta):

    def __init__(self, a, b):
        print('MyClass object with a=%s, b=%s' % (a, b))

    def foo(self, param):
        pass
    barattr = 2


foo = MyClass(1, 2)