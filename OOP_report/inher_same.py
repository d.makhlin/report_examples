class Base_1:
    def method(self):
        print("Hi i am base_1 method")


class Base_2:
    def method(self):
        print("Hi i am base 2 method")


class Base_1_1(Base_1):
    def method(self):
        print("Hi i am base_1_1 method")

# failed
class A(Base_1, Base_1_1, Base_2):
    pass
print(A.__mro__)

# class A(Base_1_1, Base_2):
#     pass
# print(A.__mro__)

# class A(Base_1, Base_2):
#     pass
# print(A.__mro__)

a = A()
print(a.method())
