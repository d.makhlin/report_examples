class A(object):
    def a_class_method(self):
        print("Method of class A")


class B(A):
    def b_class_method(self):
        print("Method of class B")


class C(A):
    def c_class_method(self):
        print("Method of class C")


class Tricky(A, B, C):
    def tricky_class_method(self):
        print('Method of class Tricky')


# lol = Tricky()
# lol.a_class_method()
# lol.b_class_method()
# lol.c_class_method()
# lol.tricky_calss_method()

print("A class")
print(A.a_class_method)
print("\nB class")
print(B.b_class_method)
print("\nC class")
print(C.a_class_method)
print(C.c_class_method)

print("\nTricky")
print(Tricky.a_class_method)
print(Tricky.b_class_method)
print(Tricky.c_class_method)
print(Tricky.tricky_class_method)

