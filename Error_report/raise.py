class MyError(Exception):
    pass

def f(a, b):
    try:
        return a / b
    except ZeroDivisionError:
        raise MyError

try:
    f(5, 0)
except MyError:
    print("Catch my error!")