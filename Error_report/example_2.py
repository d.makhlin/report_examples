class A(Exception):
    pass


class B(A):
    pass


class C(B):
    pass


for exception in [A, B, C]:
    try:
        raise exception
    except C:
        print("A")
    except B:
        print("B")
    except A:
        print("C")

    # except A:
    #     print("A")
    # except B:
    #     print("B")
    # except C:
    #     print("C")

