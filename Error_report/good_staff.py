class NoManaException(Exception):
    def __init__(self, message):
        super(NoManaException, self).__init__(message)


class Hero:
    def __init__(self):
        self.mp = 10
        self.money = 0

    def cast_spell(self, cost):
        if self.mp - cost < 0:
            raise NoManaException


hero_1 = Hero()

try:
    hero_1.cast_spell(12)
except NoManaException:
    print("Not enough mana!")