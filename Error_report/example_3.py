class CustomClass():
    def __init__(self):
        self.ordinal_color_number_of_magic = 8

    def __setattr__(self, name, value):
        if value != 8:
            raise MagicException("Magic ¯\_(ツ)_/¯")
        else:
            self.__dict__[name] = value


class MagicException(Exception):
    def __init__(self, message):
        super(MagicException, self).__init__(message)


try:
    a = CustomClass()
    a.ordinal_color_number_of_magic = 7
except MagicException:
    print("8 is true magic color number!!!")
# except MagicException:
#     print("8 is true magic color number!!!")
