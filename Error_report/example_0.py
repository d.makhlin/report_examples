# Zero division error
a = 5
b = 0

try:
    c = a / b
except ZeroDivisionError:
    print("oh no...!")


# Index error
list_a = [1, 2, 3]
index = 4

try:
    print("Mystic number is:", list_a[index])
except ValueError:
    print("Seriously...")
# except IndexError:
#     print("Come on...")
#

