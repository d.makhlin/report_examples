class MyExceptionWithContext(Exception):
    def __init___(self, dErrorArguments):
        Exception.__init__(self, "my exception was raised with arguments {0}".format(dErrorArguments))
        self.dErrorArguments = dErrorArguments


def do_stuff(a, b, c):
    if a + b + c < 0:
        raise MyExceptionWithContext({
            'a': a,
            'b': b,
            'c': c,
        })
    else:
        return print(a, b, c)

do_stuff(0, 0, -1)