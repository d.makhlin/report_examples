from multiprocessing import Process
import time as t

COUNT = 50000000


def countdown(n):
    while n > 0:
        n -= 1


if __name__ == '__main__':

    p_1 = Process(target=countdown, args=(COUNT//2,))
    p_2 = Process(target=countdown, args=(COUNT//2,))

    start = t.time()
    p_1.start()
    p_2.start()
    p_1.join()
    p_2.join()

    end = t.time()
    print('Time taken in seconds -', end - start)



# def f_1(name):
#     while True:
#         t.sleep(1)
#         print('hello', name)
#
#
# def f_2(name):
#     while True:
#         t.sleep(2)
#         print('hello', name)