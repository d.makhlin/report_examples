# multi_threaded.py
import time
from threading import Thread

COUNT = 50000000


def countdown(n):
    while n > 0:
        n -= 1


def my_input():
    print('-> ', end=" ")
    input()


t1 = Thread(target=countdown, args=(COUNT//2,))
t2 = Thread(target=countdown, args=(COUNT//2,))
t_input = Thread(target=my_input)

start = time.time()
t1.start()
t2.start()
t_input.start()

t1.join()
t2.join()
t_input.join()

end = time.time()

print('Time taken in seconds -', end - start)
