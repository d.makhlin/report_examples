# single_threaded.py
import time

COUNT = 50000000


def countdown(n):
    while n > 0:
        n -= 1


start = time.time()
countdown(COUNT//2)
print('->', end=' ')
some_input = input()
countdown(COUNT//2)
end = time.time()

print('Time taken in seconds -', end - start)
print(some_input)
