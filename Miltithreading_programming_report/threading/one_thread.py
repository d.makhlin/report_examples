# single_threaded.py
import time

COUNT = 50000000


def countdown(n):
    while n > 0:
        n -= 1
        print("У попа была собока он ее любил, она съела кусок мяса он ее убил")


start = time.time()
countdown(COUNT)
end = time.time()

print('Time taken in seconds -', end - start)
