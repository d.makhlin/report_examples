# Дополнительные материалы к докладам

1. Доклад про обработку ошибок
    - Презентация: https://docs.google.com/presentation/d/160V3ad6JnPwnJDlkBWPtmxkv8XAkPh_F1cGtY7y6ZMU/edit#slide=id.p
    - Текстовая версия доклада: NONE
2. Доклад про параллельное программирование в python
    - Презентация: https://docs.google.com/presentation/d/1IOyIHkpiw11tXS6wnPB_P253eBWDamcASoc7jrCNb8w/edit#slide=id.p
    - Текстовая версия доклада: NONE
3. Доклад про ООП в python
    - Презентация: NONE
    - Текстовая версия доклада: https://docs.google.com/document/d/1bEuGtmGPZjP6J3HhhweplN60c5LmzhA7ZqhX1c8DWCk/edit#heading=h.8cj6dy19sd2w
4. Доклад про Django ORM
    - Презентация: https://docs.google.com/presentation/d/1S9Fc3VHOSkjM9scyKzKA-vaUOYyW_ebJqjIM4lLO4es/edit#slide=id.gc6f980f91_0_0
    - Текстовая версия доклада: NONE
5. Доклад про коллекции в Python
    - Презентация: https://drive.google.com/file/d/1Am4LFnZR2rUFpGxpNr10B61KTcElBOsO/view?usp=sharing
    - Текстовая версия доклада: NONE
6. Доклад про asyncio
    - Презентация: https://docs.google.com/presentation/d/1gJK9KkK1u85xVegWukwItYUMSYC6SRY26WmEBxXDLhY/edit#slide=id.p
    - Статья по теме: https://hackernoon.com/asyncio-for-the-working-python-developer-5c468e6e2e8e
7. Доклад по Паттернам проектирования (Цепочка обязанностей и Стратегия)
    - Презентация: https://docs.google.com/presentation/d/1oNoemwRVOfj9piGF3GNBsvPH1N3Z1Ky3flG2Fu9EKV4/edit?usp=sharing
    - Текстовая версия доклада: NONE
8. Доклад по Centrifugo
    - Презентация: https://docs.google.com/presentation/d/12vjQdxR8_tCF927Z5WBV3xlzgIfZC5YhP9vYazhCqmA/edit?usp=sharing
    - Документация Centrifugo: https://centrifugal.github.io/centrifugo/server/install/
9. Доклад по возможностям PyCharm
    - Презентация: https://docs.google.com/presentation/d/1STN1Tl75ZKiehZJo89jKwiZQUw_1VEn70iv6TQEadz4/edit?usp=sharing
    - Статьи по теме:
        - https://habr.com/ru/post/226331/
        - https://www.sinyawskiy.ru/djangodebug.html
10. Доклад по NGINX
    - Презентация: https://docs.google.com/presentation/d/1R07mK8WZiFPMYMSmBnr5fnWfcghXBofFiUgmUHbe4wU/edit?usp=sharing
    - Статьи по теме:
        - https://habr.com/ru/post/260669/
        - https://habr.com/ru/post/260065/
        - https://habr.com/ru/post/259403/
        - https://habr.com/ru/post/260133/
        - http://nginx.org/ru/docs/windows.html#known_issues
11. Доклад по паттернам Builder, Stream, Event channel
    - Презентация: https://docs.google.com/presentation/d/1Wnmg56xSNasNMaNAQRvBftK8codfiL9IHopfhoNR6eE/edit?usp=sharing
    - Статьи по теме:
        - https://refactoring.guru/ru/design-patterns/builder
    - Паттерны в Django
        - Event channel: https://docs.djangoproject.com/en/2.1/topics/signals/
        - Stream: https://github.com/django/django/blob/24b82cd201e21060fbc02117dc16d1702877a1f3/django/db/models/query.py (Filters)
        - Builder: None O_o