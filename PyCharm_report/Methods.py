import math


def method_one(a, b):
    return math.sqrt(math.pow(a, 2) + math.pow(b, 2))


def method_two(item):
    try:
        int_number = int(item)
    except ValueError:
        return len(item)
    if int_number % 2 == 0:
        return True
    else:
        for counter in range(item):
            if counter > item // 2:
                return counter
