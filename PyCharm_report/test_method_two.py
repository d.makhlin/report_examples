from unittest import TestCase


class TestMethod_two(TestCase):
    def test_method_two_1(self):
        from polls.Methods import method_two
        self.assertEqual(method_two('abc'), 3)

    def test_method_two_2(self):
        debug_item = 10
        from polls.Methods import method_two
        # self.assertTrue(method_two(12))
        self.assertEqual(method_two(12), True)

    def test_method_two_3(self):
        from polls.Methods import method_two
        self.assertEqual(method_two(17), 9)
