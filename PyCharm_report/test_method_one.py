from unittest import TestCase


class TestMethod_one(TestCase):
    def test_method_one(self):
        self.assertTrue(1 == 1)

    def test_method_two(self):
        from polls.Methods import method_one
        # print(method_one(3, 4))
        self.assertEqual(method_one(3, 4), 5)
