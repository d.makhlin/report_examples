class Creature():
    def __init__(self,age):
        self.age = age

    def shout(self):
        self.age += 1
        print('I am {}'.format(self.__str__()), flush=True)


class Beast(Creature):
    def __init__(self, teeth, age):
        super().__init__(age)
        self.teeth = teeth


class Bird(Creature):
    def __init__(self, wings, age):
        super().__init__(age)
        self.wings = wings


class Wolf(Beast):
    def __init__(self, teeth, age, cubs):
        super().__init__(teeth,age)
        self.cubs = cubs


class Fox(Beast):
    def __init__(self, teeth, age, pups):
        super().__init__(teeth,age)
        self.pups = pups


Owl = Bird(4,120)
Owl.shout()
Wollo = Wolf(2, 110, 20)
Wollo.shout()
